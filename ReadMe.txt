Short summary:

My first stand-alone project. 
The application purpose - to help managing the emails, assignment to the Service Desk Agents in the current company I work. 

Application features:
1. Login and password verification; 
2. Arranging agent objects into a list; 
3. Adding/removing email numbers from an agent; 
4. Adding/removing an agent from the list; 
5. Saving results from dispatching activity to a file inside the application; 
6. Generating Excel report to the location of application file.

Technologies used:
1. Java 8; 
2. FXML language; 
3. Scene Builder; 
4. JavaFX; 
5. Apache POI library; 
6. Basic Maven dependencies;

If you want to launch application you can type in:
Login: test; 
Password: test;

More description below:


This is my first window application I created, so the resolutions I used sometimes may not be the best ones.
Please bear in mind that I am now in the middle of the Java course (in Software Development Academy)
so I do not know many features that will appear in the future lessons (like hibernate) so I used simple text files
to contain and save the work done already.

I have used various resolutions (for instance for displaying windows interface) just to practice different resolutions.


The application purpose is to help with managing email dispatching to agents (Service Desk Agents) in my current company.



The application I wrote have such features as :

Version: 1.0
1. Login panel with login verification (to assure that the application is safe),
2. Contol Panel - which contains a simple table displaying agents to whom the number of incoming emails were dispatched,
3. Saving the state of the application (making sure it will be saved for the report to be generated),
4. Managing the list of agents:
- adding new agent to the table,
- removing an agent from the table,
- Removing saved emails (which can be used after generating a report).
5. Displaying confirmation window on the close of application (Are you sure you want to close application?).

Currently the application is being tested in my current company and I am waiting for feedback of what can be added,
improved or changed.

If you will have any questions about the current code please email me on:
m.borkowski13@gmail.com,
I am open to constructive criticism.

Best regards,
Michał Borkowski.

