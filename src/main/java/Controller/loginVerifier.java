package Controller;

import java.io.*;

/**
 * This class contains loginVerification method which checks the credentials that user used to login.
 */

public class loginVerifier {


    public static boolean loginVerification(String username, String password) {
        boolean verificationResult = false;
        File file = new File("./loginFile.txt");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            while (line != null) {
                String[] fields = line.split(":");
                if (fields[0].equals(username) && fields[1].equals(password)) {
                    verificationResult = true;
                    line = reader.readLine();
                } else verificationResult = false;
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return verificationResult;
    }
}


