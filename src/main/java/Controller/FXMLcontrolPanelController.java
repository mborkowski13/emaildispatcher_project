package Controller;

import AgentManagement.AgentManagement;
import GenerateReport.ReportGenerator;
import Model.Agent;
import View.NewAgentWindow;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;


public class FXMLcontrolPanelController {

    @FXML
    private TableView<Agent> agentsTableView;

    @FXML
    private TableColumn<Agent, String> imieColumn;

    @FXML
    private TableColumn<Agent, String> nazwiskoColumn;

    @FXML
    private TableColumn<Agent, Integer> emailsColumn;


    public ObservableList<Agent> getAgents() {
        ObservableList<Agent> agents = FXCollections.observableArrayList();
        List<Agent> agentObjects = AgentManagement.getAgents();
        for (Agent agent : agentObjects) {
            agents.add(agent);
        }
        return agents;
    }

    public void addANewAgent() {
        List<String> newAgent = NewAgentWindow.returnNewAgentDetails();
        if(!newAgent.isEmpty()) {
            Agent agent = new Agent(newAgent.get(0), newAgent.get(1));
            agentsTableView.getItems().add(agent);
        }
    }

    public void removeAgentFromTableView() {
        ObservableList<Agent> agentsSelected = agentsTableView.getSelectionModel().getSelectedItems();
        ObservableList<Agent> allAgents = agentsTableView.getItems();
        if (agentsSelected.isEmpty()) {
            agentsTableView.refresh();
        } else {
            allAgents.removeAll(agentsSelected);
            agentsTableView.refresh();
        }

    }


    public ObservableList<Agent> returnAgentsInList() {
        ObservableList<Agent> changedAgents = agentsTableView.getItems();
        return changedAgents;
    }

    public void initTableView() {
        imieColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        nazwiskoColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));
        emailsColumn.setCellValueFactory(new PropertyValueFactory<>("numberOfEmails"));
        agentsTableView.setItems(getAgents());

    }

    public void addEmailButtonClicked() {
        ObservableList<Agent> agentsSelected = agentsTableView.getSelectionModel().getSelectedItems();
        if (agentsSelected.isEmpty()) {
            agentsTableView.refresh();
        } else {
            agentsSelected.get(0).setNumberOfEmails((agentsSelected.get(0).getNumberOfEmails() + 1));
            agentsTableView.refresh();
        }
    }

    public void deductEmailButtonClicked() {
        ObservableList<Agent> agentsSelected = agentsTableView.getSelectionModel().getSelectedItems();
        agentsTableView.refresh();
        if (agentsSelected.isEmpty()) {
            agentsTableView.refresh();
        } else {
            agentsSelected.get(0).setNumberOfEmails((agentsSelected.get(0).getNumberOfEmails() - 1));
            agentsTableView.refresh();
        }
    }

    public void saveChangesMadeToAgents() {
        ObservableList<Agent> agentsInList = returnAgentsInList();
        File file = new File("./agentDetails.txt");
        StringWriter sw = new StringWriter();
        for (Agent agent : agentsInList) {
            sw.append("id:" + agent.getId() + ",");
            sw.append("name:" + agent.getName() + ",");
            sw.append("surname:" + agent.getSurname() + ",");
            sw.append("numberOfEmails:" + agent.getNumberOfEmails() + ",");
            sw.append("\n");
        }
        FileWriter fw = null;
        try {
            fw = new FileWriter(file);
            fw.write(sw.toString());
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void resetAgentsEmails() {
        ObservableList<Agent> allAgents = agentsTableView.getItems();
        for (int i = 0; i < allAgents.size(); i++) {
            allAgents.get(i).setNumberOfEmails(0);
            agentsTableView.refresh();
        }
    }

    public void generateExcelReport(ActionEvent event) {
        saveChangesMadeToAgents();
        ReportGenerator.generateExcelReport();
    }
}
