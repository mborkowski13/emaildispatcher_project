package View;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class NewAgentWindow {

    /**
     * Window with two text fields where user puts details of the new agent which is to be added to the list of emails.
     * @return List of Strings which contains two fields - [0] - name of the agent, and [1] - surname of the agent.
     */


    public static List<String> returnNewAgentDetails() {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Create New Agent");
        window.setMinWidth(250);

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);


        Label nameLabel = new Label("Name:");
        GridPane.setConstraints(nameLabel, 0, 0);

        TextField nameInput = new TextField("Name");
        GridPane.setConstraints(nameInput, 1, 0);


        Label surnameLabel = new Label("Surname:");
        GridPane.setConstraints(surnameLabel, 0, 1);

        TextField surnameInput = new TextField("Surname");
        GridPane.setConstraints(surnameInput, 1, 1);


        Button acceptButton = new Button("Add agent");
        GridPane.setConstraints(acceptButton, 1, 2);

        List<String> newAgent = new ArrayList<>();
        acceptButton.setOnAction(e -> {
            newAgent.add(nameInput.getText());
            newAgent.add(surnameInput.getText());
            window.close();
        });

        grid.getChildren().addAll(nameLabel, nameInput, surnameLabel, surnameInput, acceptButton);

        Scene scene = new Scene(grid, 300, 200);
        window.setScene(scene);
        window.showAndWait();
        return newAgent;
    }
}
