package View;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class ConfirmationWindow {

    /**
     * Window which appears when the user clicks on the x button in the right corner of the window (Windows system).
     *
     * @return Boolean variable which says true - when user wants to shut down the application
     * or false - when user did it unintentionally.
     */


    public static Boolean displayConfirmationWindow() {
        final boolean[] choice = {false};

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Confirm");
        window.setMinWidth(250);


        Label label = new Label("Are you sure you want to exit? \n Remember to SAVE the work.");

        Button yesButton = new Button("Yes");
        yesButton.setOnAction(event ->{
            choice[0] =true;
            window.close();
        });

        Button noButton = new Button("No");
        noButton.setOnAction(event -> window.close());

        VBox layout = new VBox();
        layout.getChildren().addAll(label, yesButton, noButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        boolean finalChoice = choice[0];
        return finalChoice;
    }
}
