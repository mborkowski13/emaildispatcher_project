package View;

import Controller.FXMLcontrolPanelController;
import Controller.loginVerifier;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

import static View.ConfirmationWindow.displayConfirmationWindow;

/**
 *
 *
 * @author Michał Borkowski
 * @version 1.0
 *
 * This method contains two classes.
 *
 *1. Start - where the logon screen appears which is loaded from FXML file.
 *2. loadControlPanel - it loads the control panel for managing emails, when the logon is successful.
 *
 * User interface contains also reference to FXML objects with are used for logging in to the main application.
 *
 * FXMLcontrolPanelController for Control Panel is located in FXMLcontrolPanelController.FXMLcontrolPanelController package.
 */

public class UserInterface extends Application {

    @FXML
    private TextField loginTxtField;

    @FXML
    private PasswordField passTextField;

    @FXML
    private TextArea messageBox;

    @FXML
    private Button loginButton;


    public static void main(String[] args) {
        launch(args);
    }

    /**
     *
     * This method loads login screen which verifies the login credentials.
     * @param primaryStage
     * @throws Exception
     */

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/LoginPage.fxml"));
            root = loader.load();
            Scene loginScene = new Scene(root);
            primaryStage.setScene(loginScene);
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * If the login is successful then this method loads Control Panel.
     * @throws IOException
     */


    @FXML
    public void loadControlPanel() throws IOException{
        Stage stage;
        Parent root;
        try {
            if (loginVerifier.loginVerification(loginTxtField.getText(), passTextField.getText())){
                stage =(Stage)loginButton.getScene().getWindow();
                FXMLLoader load = new FXMLLoader(getClass().getResource("/ControlPanel.fxml"));
                root = load.load();
                FXMLcontrolPanelController FXMLcontrolPanelController = load.getController();
                FXMLcontrolPanelController.initTableView();
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.setResizable(true);
                stage.show();
                stage.setOnCloseRequest(event -> {
                    event.consume();
                        Boolean answer = displayConfirmationWindow();
                        if(answer) {
                            stage.close();
                        }
                });

        } else {
                messageBox.setStyle("-fx-text-inner-color: red;");
                messageBox.setText("Wrong password or login.");
            }
        } catch (IOException e ){
            messageBox.setText("Wrong password or login.");

        }
    }
}
