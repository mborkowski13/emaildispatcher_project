package Model;

/**
 * Simple Agent model with a few data fields.
 */

public class Agent {
    private String name;
    private String surname;
    private int numberOfEmails;
    private int id;
    private int staticId = 1;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getNumberOfEmails() {
        return numberOfEmails;
    }

    public void setNumberOfEmails(int numberOfEmails) {
        this.numberOfEmails = numberOfEmails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Agent() {
        this.id = staticId;
        staticId++;
        name = "";
        surname = "";
        numberOfEmails = 0;
    }

    public Agent(String name, String surname) {
        this.id = staticId;
        staticId++;
        this.name = name;
        this.surname = surname;
        numberOfEmails = 0;
    }

    @Override
    public String toString() {
        return "Agent{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", numberOfEmails=" + numberOfEmails +
                '}';
    }
}
