package AgentManagement;

import Model.Agent;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AgentManagement {

    /**
     * The method loads agents from agentDetails file.
     *
     * @return List of Agent objects.
     */


    public static List<Agent> getAgents() {
        File file = new File("./agentDetails.txt");
        List<Agent> listOfAgents = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            while (line != null) {
                String[] pola = line.split(",");
                Agent agent = new Agent();
                for (String pole : pola) {
                    String[] nazwaWartosc = pole.split(":");
                    switch (nazwaWartosc[0]) {
                        case "id":
                            agent.setId(Integer.parseInt(nazwaWartosc[1]));
                            break;
                        case "name":
                            agent.setName(nazwaWartosc[1]);
                            break;
                        case "surname":
                            agent.setSurname(nazwaWartosc[1]);
                            break;
                        case "numberOfEmails":
                            agent.setNumberOfEmails(Integer.parseInt(nazwaWartosc[1]));
                            break;
                    }
                }
                listOfAgents.add(agent);
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfAgents;
    }
}


