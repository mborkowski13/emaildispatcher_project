package GenerateReport;

import AgentManagement.AgentManagement;
import Model.Agent;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ReportGenerator {

    /**
     * This method generates report in Excel file in the location where
     */

    public static void generateExcelReport() {
        try {
            LocalDateTime date = LocalDateTime.now();
            List<Agent> agents = AgentManagement.getAgents();

            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("Emails report");
            Row heading = sheet.createRow(0);
            // Create heading for the report
            heading.createCell(0).setCellValue("Name");
            heading.createCell(1).setCellValue("Surname");
            heading.createCell(2).setCellValue("Number of emails");
            // Put values to the proper columns using loop
            for (int i = 0; i < 3; i++) {
                CellStyle styleRowHeading = workbook.createCellStyle();
                Font font = workbook.createFont();
                font.setBold(true);
                font.setFontName(HSSFFont.FONT_ARIAL);
                font.setFontHeightInPoints((short) 11);
                styleRowHeading.setFont(font);
                styleRowHeading.setVerticalAlignment(VerticalAlignment.CENTER);
                heading.getCell(i).setCellStyle(styleRowHeading);
            }
            int r = 1;
            for (Agent agent : agents) {
                Row row = sheet.createRow(r);
                //Name Colum
                Cell nameCell = row.createCell(0);
                nameCell.setCellValue(agent.getName());
                //Surname Column
                Cell surnameCell = row.createCell(1);
                surnameCell.setCellValue(agent.getSurname());
                //Number of Emails
                Cell numOfEmails = row.createCell(2);
                numOfEmails.setCellValue(agent.getNumberOfEmails());
                r++;
            }
            //Autofit
            for (int i = 0; i < 3; i++) {
                sheet.autoSizeColumn(i);
            }
            //SAVE to Excel File
            FileOutputStream out = new FileOutputStream(new File("./" + date.format(DateTimeFormatter.ISO_LOCAL_DATE) + ".xls"));
            try {
                workbook.write(out);
                out.close();
                workbook.close();
                System.out.println("Excel written successfully.");
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
